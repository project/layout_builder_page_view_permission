<?php

namespace Drupal\layout_builder_page_view_permissions;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\node\Entity\NodeType;

/**
 * Provides permissions for layout builder pages.
 */
class LayoutBuilderPageViewPermissionsPermissions {
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function permissions() {
    $permissions = [];
    $nodeTypes = NodeType::loadMultiple();
    foreach ($nodeTypes as $nodeType) {
      if ($nodeType->id() == 'layout_builder_page') {
        $permission = 'view any page of ' . $nodeType->id() . ' content';
        $permissions[$permission] = [
          'title' => $this->t('<em>@type_label</em>: View any page content', ['@type_label' => $nodeType->label()]),
        ];
        $permission = 'view own page of ' . $nodeType->id() . ' content';
        $permissions[$permission] = [
          'title' => $this->t('<em>@type_label</em>: View own page content', ['@type_label' => $nodeType->label()]),
        ];
      }
    }
    return $permissions;
  }

}
