CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Layout Builder Page View Permission module intends to provide the page view 
permission for the user based on the permission given for his/her role

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/layout_builder_page_view_permissions

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/search/layout_builder_page_view_permissions


REQUIREMENTS
------------

Layout Builder Module and Node module needs to be installed to 
make this module work.


RECOMMENDED MODULES
-------------------

No Recommended modules

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.



CONFIGURATION
-------------

Provide required permission based on the role for layout builder pages.


MAINTAINERS
-----------

Current maintainers:
 * Anirban Deb - https://www.drupal.org/u/adebdrupal
